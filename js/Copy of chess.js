
//These two arrary are 8 elements wide each, making it convenient for loops
var letterArray = new Array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h');
var pieceArray = new Array('R', 'N', 'B', 'Q', 'K', 'B', 'N', 'R');

var paleteArray = new Array('R', 'N', 'B', 'Q', 'K', 'P');
//The first letter denotes color
var allPieceArray = new Array('WR', 'WN', 'WB', 'WQ', 'WK', 'WP','BR', 'BN', 'BB', 'BQ', 'BK', 'BP');
var colorArray = new Array('W', 'B');

var imagesSrcPath = 'images/';


/********************
Function getImgSrcHtml
Returns the HTML for the piece IMG tage
squareLetter - uses letterArray
piece - uses pieceArray types 
color - B or W - need toUpper on id because fileName uses lowerCase
********************/
function getImgSrcHtml(squareLetter, col, piece, color) 
{
	var htmlText = "<img id='"+squareLetter+col+color.toUpperCase()+piece+"' class=draggableImg src='"+imagesSrcPath+color+piece+".png'"+"/>";
	return htmlText;
}

/********************
Function setStartingPosition
********************/
function setStartingPosition() {
	
	var l=0;
	//set empty squares in middle
	for(var i=3; i <= 6; i+=1) {
		for(l=0; l<letterArray.length; l+=1) {
			$("#"+letterArray[l]+""+i).html("");
		}
	}
	//set pawns
	for(l=0; l<letterArray.length; l+=1) {
		$("#"+letterArray[l]+"7").html(getImgSrcHtml(letterArray[l], '7', 'P', 'b'));
		$("#"+letterArray[l]+"2").html(getImgSrcHtml(letterArray[l], '2', 'P', 'w'));
	}
	
	
	//pieces
	for(l=0; l<letterArray.length; l+=1) {
		$("#"+letterArray[l]+"1").html(getImgSrcHtml(letterArray[l], '1', pieceArray[l], 'w'));
		$("#"+letterArray[l]+"8").html(getImgSrcHtml(letterArray[l], '8', pieceArray[l], 'b'));
	}
	setInitialPiecesDraggable();
}



/********************
Function clearBoard
********************/
function clearBoard()
	{
		var divId = "";
		for (row = 8; row > 0; row -= 1) {
			
			for (col = 1; col <= 8; col+=1) {
				divId = getSquareId(row, col);
				$("#"+divId).html('');
			}
		}
	}


/********************
Function setFenToBoard
********************/
function setFenToBoard()
	{
		var fenString = getFenField();
		var fenArray = new Array();
		fenArray = fenString.split('/');
		 
		var divId = "";
		var pieces="";
		var piece="";
		var blankSquareCount = 0;
		var arrayRowPos=0;
		var arrayColPos=0;
		clearBoard();
		for (row = 8; row > 0; row -= 1) {
			pieces = fenArray[arrayRowPos];
			for (col = 1; col <= 8; col+=1) {
				divId = getSquareId(row, col);
				piece = pieces.charAt(arrayColPos);
				arrayColPos += 1;
				if( !isLetter(piece) && !isDigit(piece) )
				{
					break;
				}
				if(isDigit(piece)) {
					col = col + (parseInt(piece)) - 1;
					continue;
				}
				$("#"+divId).html(getImgSrcHtml(letterArray[col-1], row.toString(), piece.toUpperCase(), getFenColor(piece)));
			}
			arrayColPos=0;
			arrayRowPos += 1;
		}
		setFenField();
		return;
	}


/********************
Function getFenFromBoard
********************/
function getFenFromBoard()
	{
		var fenString = "";
		var divId = "";
		var piece="";
		var blankSquareCount = 0;
		for (row = 8; row > 0; row -= 1) {
			if (blankSquareCount > 0) {
				fenString += blankSquareCount + "";
				blankSquareCount = 0;
			}
			if (row < 8 && row >= 1) {
				fenString+="/";
			}
			for (col = 1; col <= 8; col+=1) {
				divId = getSquareId(row, col);
				
				piece = $('#'+divId+' img').attr('id'); 
				if(piece == undefined || piece == '') {
					blankSquareCount += 1;
					continue;
				}
				
                if(piece.length == 4) {
                    piece = piece.substring(2, 4);
                } else if (piece.length == 3) {
                	piece = piece.substring(1, 3);
                } else {
                    piece = '';
                }
				
				if (piece == '') {
					blankSquareCount += 1;
				} else {
					if (blankSquareCount > 0) {
						fenString += blankSquareCount + "";
						blankSquareCount = 0;
					}

					if(piece.charAt(0) == 'B') {
						fenString += piece.charAt(1).toLowerCase();
					} else {
						fenString += piece.toUpperCase().charAt(1); 
					}
				}
				
			}
		}
		if (blankSquareCount > 0) {
			fenString += blankSquareCount + "";
			blankSquareCount = 0;
		}
		return fenString;
	}



/********************
Function getFenColor
returns the correct color case, b or w 
depending on the case of the piece letter
********************/
function getFenColor(achar) {
	if( (achar.charCodeAt() >= 65 && achar.charCodeAt() <= 90) )
	{
		return 'w';
	}
	if( achar.charCodeAt() >= 97 && achar.charCodeAt() <= 122 ) 
	{
		return 'b';
	}
}


/********************
Function isLetter
********************/
function isLetter(achar) {
	return ( (achar.charCodeAt() >= 65 && achar.charCodeAt() <= 90) ||
			(achar.charCodeAt() >= 97 && achar.charCodeAt() <= 122) );
}


/********************
Function isDigit
********************/
function isDigit(achar) {
	return ( (achar.charCodeAt() >= 48 && achar.charCodeAt() <= 57));
}


/********************
Function getSquareId
********************/
function getSquareId(row, col) 
{
	var divId = "";
	divId = letterArray[col-1] + (row+"");
	return divId; 
}



/********************
	Function init
 ********************/
function init() 
{
	$("ul.dropdown li").hover(function(){
	    
        $(this).addClass("hover");
        $('ul:first',this).css('visibility', 'visible');
    
    }, function(){
    
        $(this).removeClass("hover");
        $('ul:first',this).css('visibility', 'hidden');
    
    });
    
    $("ul.dropdown li ul li:has(ul)").find("a:first").append(" &raquo; ");
	initWidgets();
	setInitialPiecesDraggable();
	setSquaresDroppable ();
	setFenField();
}

/********************
Function initWidgets
********************/
function initWidgets() {

	$( "#okButton" ).bind( {
		click: handleClickOkButton
	});
	/////////////////  Menu  ///////////////////
	$('#m1').click(function() {
		handleClearButton();
		});
	
	$('#m2').click(function() {
		handleResetButton();
		});
	
	$('#m3').click(function() {
		setFenToBoard();
		});
	
	$('#m4').click(function() {
		preview();
		});

	
	
	
	
}

/********************
Function handleResetButton
********************/
function handleResetButton() {
	setStartingPosition();
	boardUpdateAfter();
}

/********************
Function handleClearButton
********************/
function handleClearButton() {
	clearBoard();
	boardUpdateAfter();
}

/********************
Function preview
********************/
function preview() {
	handleClickOkButton('preview');
}

/********************
Function handleClickOkButton
********************/
function handleClickOkButton(mode) {
	
	var previewMode=false;
	if(mode == 'preview') { 
		previewMode = true;
	}
		
	//Returns text back to parent
	//parent is phpBBS posting window
	//so this calls editor.js method insert_text
	if( previewMode || !(window.opener == null)) {
		var myDate = new Date();
		
		var posterId = $('#posterId').val();
		var postId = $('#postId').val(); 
		var topicId = $('#topicId').val();
		var forumId = $('#forumId').val();
		
		var fileNameString = "chess_diagram_"+myDate.getTime()+".png";

		var fenString = getFenFromBoard();
		
		if(!previewMode) {
			var insertText = "[attachment=0]"+fileNameString+"[/attachment]";
			window.opener.insert_text(insertText, true, false); //call phpbb method
		}
		
		var coordinatesCB;
		if($('#coordinatesCB').is(':checked') == true) {
			coordinatesCB = 'on';
		} else {
			coordinatesCB = 'off';
		}
		
		//alert('red: ' + rgb.r + ', green: ' + rgb.g + ', blue: '+rgb.b);
		//&ds_color=(0,0,255) ls_color=(255,200,200) &outline_color=(255,0,0) &border_width=5
		var rgb=$.jPicker.List[0].color.active.val('rgb');
		var ds_color = '('+rgb.r+','+rgb.g+','+rgb.b+')';
		
		var rgb=$.jPicker.List[1].color.active.val('rgb');
		var ls_color = '('+rgb.r+','+rgb.g+','+rgb.b+')';
		
		var pngUrl;
		pngUrl = getUrlRootLocation()+"/createAndSaveImage.php";
		//window.open(pngUrl, 'Chess Diagram Editor', false);
		//http://www.stlchess.com/dbAndWriteTest.php/createAndSaveImage.php?fen=8/8/8/7q/8/5k1K/8/8&fileName:xyz.png&posterId=999&postId=999&postId=999&topicId=999&forumId=999

		
		if(previewMode) 
		{
			previewParms='?postId='+postId+
			'&posterId='+posterId+
			'&topicId='+topicId+
			'&forumId='+forumId+
			'&fen='+fenString+
			'&fileName='+fileNameString+
			'&coordinates='+coordinatesCB+
			'&ds_color='+ds_color+ 
			'&ls_color='+ls_color+
			'&previewMode='+previewMode;
			
			//window.open(pngUrl);
			var str = "<br><center><img src='" + pngUrl + "?" +previewParms + "'></center>";
			var h = coordinatesCB == 'on' ? 400 : 350;
			$.window({
				   title: "Chess Diagram Preview",
				   draggable: false,
				   resizable: false,
				   maximizable: false,
				   minimizable: false,
				   showModal: true,
				   height: h,
				   x: 100,
				   y: 100,
				   content: str
				});
		} 
		else 
		{
			jQuery.ajax({
				type: "GET",
		         url:    pngUrl,
		         success: function(result) {
		                      if(result.isOk == false)
		                          alert(result.message);
		                  },
		         data: {fen: fenString, fileName: fileNameString, posterId: posterId, postId: postId, topicId: topicId, 
		                	  forumId: forumId, coordinates: coordinatesCB, ds_color: ds_color, ls_color: ls_color, previewMode: previewMode},
		         async:   false
		    });
		 window.close();
		}
	
		
	} else {
		alert(getFenFromBoard());
		alert("This returns back to parent window but there is no parent!");
	}
}

/********************
Function getUrlRootLocation
returns absolute url position
********************/
function getUrlRootLocation () 
{
	var scriptFileLocation = "phpbb"; //default of remote phpbb folder where phpbb root is located.
	var location = "";
	if(window.location.host == 'localhost') {
		//Xamp points to the subfolder as the root already
		scriptFileLocation = '';
	}
	location = window.location.protocol+"//"+window.location.host+"/"+scriptFileLocation;
	
	return location;
}


/********************
	Function setSquaresDroppable
 ********************/
function setSquaresDroppable ()
{
	$('.whiteSpace').droppable( {
		drop: handleDropEventMainBoard
		} );
	$('.blackSpace').droppable( {
		drop: handleDropEventMainBoard
		} );
	$('.paleteSpace').droppable( {
		drop: handleDropEventPalete
		} );
	
}

/********************
	Function addDraggable
	Double click a piece will put that into drag mode
 ********************/
function addDraggable(event, ui) 
{	
		$(this).draggable( {
			cursor: 'move',
			snap: '.space',
			helper: 'clone'
			} );
	
}


/********************
	Function setInitialPiecesDraggable
	Initializes the double click event on the pieces
 ********************/
function setInitialPiecesDraggable() 
{
	
	$( ".paletteImg" ).draggable({ cursor: 'move', snap: '.space', helper: 'clone' });
	$( ".draggableImg" ).draggable({ cursor: 'move', snap: '.space', helper: 'clone' });

	
	//Make pieces draggable when double clicking
//	for(var i=0; i < pieceArray.length; i+=1) 
//	{
//		for(var j=0; j < 2; j+=1) 
//		{
//			$("#"+letterArray[i]+(i+"")+colorArray[j]+"P").draggable({ cursor: 'move', snap: '.space', helper: 'clone' });
//			
//			$("#"+letterArray[i]+(i+"")+colorArray[j]+pieceArray[i]).draggable({ cursor: 'move', snap: '.space', helper: 'clone' });
//			
////			$("#"+letterArray[i]+colorArray[j]+"P").bind( {
////				click: addDraggable
////				} );
////
////			$("#"+letterArray[i]+colorArray[j]+pieceArray[i]).bind( {
////				click: addDraggable
////				} );
//		}
//	}
	
//	for(var i=0; i < paleteArray.length; i+=1)
//	{
//		$("#p"+"W"+paleteArray[i]).bind( {
//			click: addDraggable
//		} );
//		$("#p"+"B"+paleteArray[i]).bind( {
//			click: addDraggable
//		} );
//	}
}


/********************
	Function handleDropEventPalete
	Handles removing a piece from the board
	********************/
function handleDropEventPalete( event, ui ) 
{
	var draggable = ui.draggable;
	
	var squareToId = $(this).attr("id");
	var pieceId = draggable.attr("id");
	var squareFromId = draggable.parent().attr("id");
	var isPaletePiece = pieceId.charAt(0) == 'p';
	
	ui.draggable.draggable( 'stop' );
	if( !isPaletePiece ) {
		$("#"+squareFromId).html('');
	}
	//change the cursor back
	$(document.body).css( 'cursor', 'auto' );
	
	boardUpdateAfter();
	
}


/********************
	Function handleDropEventMainBoard
	A piece can come from the board or palete
 ********************/
function handleDropEventMainBoard( event, ui ) 
{
		var draggable = ui.draggable;
	
	var squareToId = $(this).attr("id");
	var pieceId = draggable.attr("id");
	var isPaletePiece = pieceId.charAt(0) == 'p';
	var squareFromId = draggable.parent().attr("id");
	
	//change the cursor back
	$(document.body).css( 'cursor', 'default' );
	
	ui.draggable.draggable( 'stop' );
	
	//ui.draggable.draggable( 'disable' );
		//alert( 'The piece with ID "' + pieceId + '" was dropped onto: '+squareToId + " Parent: "+squareFromId);

	if(squareToId == squareFromId)
	{
		return;
	}
	//src with relative path
	//var imageSrc = $("#"+pieceId).attr("src");
	//src with absolute path
	var imageSrc = $("#"+pieceId).get(0).src;

	//Update the pieceId if from palete - piece ids on pallete are 3 chars, on board 4 chars
	if( isPaletePiece == true ) 
	{
		pieceId = squareToId.charAt(0)+squareToId.charAt(1)+pieceId.charAt(1)+pieceId.charAt(2);
	} else {
		pieceId = squareToId.charAt(0)+squareToId.charAt(1)+pieceId.charAt(2)+pieceId.charAt(3);
	}
	var htmlText = "<img id='"+pieceId+"' class=draggableImg src='"+imageSrc+"'/>";
	$("#"+squareToId).html(htmlText);
	if( isPaletePiece == false )
	{
		$("#"+squareFromId).html('');
	}

	//Add the event to the "new" piece
	$( "#"+pieceId ).draggable({ cursor: 'move', snap: '.space', helper: 'clone' });
	
	boardUpdateAfter();
	 
}

/********************
Function setFenField
Put the FEN text into the FEN field
********************/
function setFenField() {
	$("#inputFen").val(getFenFromBoard());
}

/********************
Function getFenField
Put the FEN text into the FEN field
********************/
function getFenField() {
	return $("#inputFen").val();
}

/********************
Function boardUpdateAfter
Called by methods that update the board at the end of their function
********************/
function boardUpdateAfter() {
	setFenField();
}
