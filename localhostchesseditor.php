<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	
	<title>Chess Diagram Maker</title>
	<link rel="stylesheet" href="css/chessDiagram.css">
	<link rel="stylesheet" href="jquery-ui-1.8.16/themes/base/jquery.ui.all.css">
	
	<script src="js/chess.js"></script>
	
	<script src="jquery-ui-1.8.16/jquery-1.6.2.js" type="text/javascript"></script>
	<script src="jquery-ui-1.8.16/themes/base/jquery.ui.core.js"></script>

	<script src="jquery-ui-1.8.16/themes/base/jquery.ui.widget.js"></script>
	<script src="jquery-ui-1.8.16/themes/base/jquery.ui.mouse.js"></script>
	<script src="jquery-ui-1.8.16/themes/base/jquery.ui.draggable.js"></script>
	<script src="jquery-ui-1.8.16/themes/base/jquery.ui.droppable.js"></script>

	<script>
    //Call init
	$(document).ready(function(){
		$( init );
	});
	</script>
	
</head>
<body>

<?php

	require( "/config.php" );

  	$posterId = $_GET['posterId'];
	$postId = $_GET['postId'];
	$topicId = $_GET['topicId'];
	$forumId = $_GET['forumId'];
  	

	echo "<input type='hidden' id='postId' name='postId' value=" . $postId . " />";	
	echo "<input type='hidden' id='posterId' name='posterId' value=" . $posterId . " />";
	echo "<input type='hidden' id='topicId' name='topicId' value=" . $topicId . " />";
	echo "<input type='hidden' id='forumId' name='forumId' value=" . $forumId . " />";

?>



<ul class="dropdown">
        	<li><a href="#">Chess Diagram Menu</a>
        		<ul class="sub_menu">
        			 <li id="m1">
        			 	<a href="#">Clear Board</a>
        			 </li>
        			 <li id="m2">
        			 	<a href="#">Reset Board</a></li>
        			 </li>
        		</ul>
        	</li>
    
	</ul>
<div id="OuterWrapper">
	
     <div id="piecepalete">
		<div id="white">
		<div class="space paleteSpace" id="p1"><img class="paletteImg" id="pBP" src="images/bP.png"/></div>
        <div class="space paleteSpace" id="p2"><img class="paletteImg" id="pBR" src="images/bR.png"/></div>
        <div class="space paleteSpace" id="p3"><img class="paletteImg" id="pBN" src="images/bN.png"/></div>
        <div class="space paleteSpace" id="p4"><img class="paletteImg" id="pBB" src="images/bB.png"/></div>
        <div class="space paleteSpace" id="p5"><img class="paletteImg" id="pBQ" src="images/bQ.png"/></div>
        <div class="space paleteSpace" id="p6"><img class="paletteImg" id="pBK" src="images/bK.png"/></div>
		</div>
		<div id="black">
		<div class="space paleteSpace" id="p7"><img class="paletteImg" id="pWP" src="images/wP.png"/></div>
        <div class="space paleteSpace" id="p8"><img class="paletteImg" id="pWR" src="images/wR.png"/></div>
        <div class="space paleteSpace" id="p9"><img class="paletteImg" id="pWN" src="images/wN.png"/></div>
        <div class="space paleteSpace" id="p10"><img class="paletteImg" id="pWB" src="images/wB.png"/></div>
        <div class="space paleteSpace" id="p11"><img class="paletteImg" id="pWQ" src="images/wQ.png"/></div>
        <div class="space paleteSpace" id="p12"><img class="paletteImg" id="pWK" src="images/wK.png"/></div>
		</div>
	</div> <!-- end piecepalete -->
	

	<div id="boardMargin">
        		
        		<div id="numbers">
          			<div class="number">8</div>
          			<div class="number">7</div>
          			<div class="number">6</div>
          			<div class="number">5</div>
          			<div class="number">4</div>
          			<div class="number">3</div>
          			<div class="number">2</div>
          			<div class="number">1</div>
        		</div>
        		<div id="boardWrapper">
          			<div class="space whiteSpace" id="a8"><img id="aBR" src="images/bR.png"/></div>
          			<div class="space blackSpace" id="b8"><img id="bBN" src="images/bN.png"/></div>
          			<div class="space whiteSpace" id="c8"><img id="cBB" src="images/bB.png"/></div>
          			<div class="space blackSpace" id="d8"><img id="dBQ" src="images/bQ.png"/></div>
          			<div class="space whiteSpace" id="e8"><img id="eBK" src="images/bK.png"/></div>
          			<div class="space blackSpace" id="f8"><img id="fBB" src="images/bB.png"/></div>
          			<div class="space whiteSpace" id="g8"><img id="gBN" src="images/bN.png"/></div>
          			<div class="space blackSpace" id="h8"><img id="hBR" src="images/bR.png"/></div>
          			<!-- end first row -->
          			<div class="space blackSpace" id="a7"><img id="aBP" src="images/bP.png"/></div>
          			<div class="space whiteSpace" id="b7"><img id="bBP" src="images/bP.png"/></div>
          			<div class="space blackSpace" id="c7"><img id="cBP" src="images/bP.png"/></div>
          			<div class="space whiteSpace" id="d7"><img id="dBP" src="images/bP.png"/></div>
          			<div class="space blackSpace" id="e7"><img id="eBP" src="images/bP.png"/></div>
          			<div class="space whiteSpace" id="f7"><img id="fBP" src="images/bP.png"/></div>
          			<div class="space blackSpace" id="g7"><img id="gBP" src="images/bP.png"/></div>
          			<div class="space whiteSpace" id="h7"><img id="hBP" src="images/bP.png"/></div>
          			<!-- end second row -->
          			<div class="space whiteSpace" id="a6"> </div>
          			<div class="space blackSpace" id="b6"> </div>
          			<div class="space whiteSpace" id="c6"> </div>
          			<div class="space blackSpace" id="d6"> </div>
          			<div class="space whiteSpace" id="e6"> </div>
          			<div class="space blackSpace" id="f6"> </div>
          			<div class="space whiteSpace" id="g6"> </div>
          			<div class="space blackSpace" id="h6"> </div>
          			<!-- end third row -->
          			<div class="space blackSpace" id="a5"> </div>
          			<div class="space whiteSpace" id="b5"> </div>
          			<div class="space blackSpace" id="c5"> </div>
          			<div class="space whiteSpace" id="d5"> </div>
          			<div class="space blackSpace" id="e5"> </div>
          			<div class="space whiteSpace" id="f5"> </div>
          			<div class="space blackSpace" id="g5"> </div>
          			<div class="space whiteSpace" id="h5"> </div>
          			<!-- end fourth row -->
          			<div class="space whiteSpace" id="a4"> </div>
          			<div class="space" id="b4"> </div>
          			<div class="space whiteSpace" id="c4"> </div>
          			<div class="space blackSpace" id="d4"> </div>
          			<div class="space whiteSpace" id="e4"> </div>
          			<div class="space blackSpace" id="f4"> </div>
          			<div class="space whiteSpace" id="g4"> </div>
          			<div class="space blackSpace" id="h4"> </div>
          			<!-- end fifth row -->
          			<div class="space blackSpace" id="a3"> </div>
          			<div class="space whiteSpace" id="b3"> </div>
          			<div class="space blackSpace" id="c3"> </div>
          			<div class="space whiteSpace" id="d3"> </div>
          			<div class="space blackSpace" id="e3"> </div>
          			<div class="space whiteSpace" id="f3"> </div>
          			<div class="space blackSpace" id="g3"> </div>
          			<div class="space whiteSpace" id="h3"> </div>
          			<!-- end sixth row -->
          			<div class="space whiteSpace" id="a2"><img id="aWP" src="images/wP.png"/></div> 
		  			<div class="space blackSpace" id="b2"><img id="bWP" src="images/wP.png"/></div> 
		  			<div class="space whiteSpace" id="c2"><img id="cWP" src="images/wP.png"/></div> 
		  			<div class="space blackSpace" id="d2"><img id="dWP" src="images/wP.png"/></div> 
		  			<div class="space whiteSpace" id="e2"><img id="eWP" src="images/wP.png"/></div> 
		  			<div class="space blackSpace" id="f2"><img id="fWP" src="images/wP.png"/></div> 
		  			<div class="space whiteSpace" id="g2"><img id="gWP" src="images/wP.png"/></div> 
		  			<div class="space blackSpace" id="h2"><img id="hWP" src="images/wP.png"/></div> 

          			<!-- end seventh row -->
          			<div class="space blackSpace" id="a1"><img id="aWR" src="images/wR.png"/></div>
          			<div class="space whiteSpace" id="b1"><img id="bWN" src="images/wN.png"/></div>
          			<div class="space blackSpace" id="c1"><img id="cWB" src="images/wB.png"/></div>
          			<div class="space whiteSpace" id="d1"><img id="dWQ" src="images/wQ.png"/></div>
          			<div class="space blackSpace" id="e1"><img id="eWK" src="images/wK.png"/></div>
          			<div class="space whiteSpace" id="f1"><img id="fWB" src="images/wB.png"/></div>
          			<div class="space blackSpace" id="g1"><img id="gWN" src="images/wN.png"/></div>
          			<div class="space whiteSpace" id="h1"><img id="hWR" src="images/wR.png"/></div>
          			<!-- end eighth row -->
        		</div> <!-- end boardWrapper -->
        		<div id="boardLettersTop">
          			<div class="letterClass">a</div>
          			<div class="letterClass">b</div>
          			<div class="letterClass">c</div>
          			<div class="letterClass">d</div>
          			<div class="letterClass">e</div>
          			<div class="letterClass">f</div>
          			<div class="letterClass">g</div>
          			<div class="letterClass">h</div>
          			<div class="clear"></div>
        		</div>
</div> <!-- BoardMargin-->

<div id="controls_container">
	<form>
		<button id="okButton" class="ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all">
   			<span class="ui-button-text">OK</span>
		</button>
		<span id="fenLabel">FEN: </span>
		<input id="inputFen" type="text" />
	</form>
</div>
</div> <!-- OuterWrapper -->

</body>
</html>
