This software is governed by the license found in GPL-LICENSE.txt.

Copyright (c) 2013 Timothy C. Nesham Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to 
whom the Software is furnished to do so, subject to the following conditions: 
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

Working example: http://www.nlightening.com/test/chesseditor.php

The diagram maker was originally written for a PHPBBS board called stlchess.com (RIP). The source is independent of the PHPBBS hooks I added to 
work with this diagram maker.
Original user (phpbb) documentation can be found at: http://www.nlightening.com/test/ChessDiagramMakerDocumentation.html

The PNG generator (written in PHP) was written by Copyright (c) 2009 Steven L. Eddins, and has the same copyright notice as above.


Overview of Files:

chesseditor.php - contains the html - (no html tables, just divs). The DIV board was originally conceived and written by Scott A. Nesham,
who is a CSS master. After that it was rewritten a few times, but I wanted to mention my son. <grin> 

chess.js - The main JQuery code.
chessDiagram.css - css

All other files such as Jquery have their own copyright rules, which are in the public domain.






